<?php
/**
 * Created by PhpStorm.
 * User: eugene.kuzob
 * Date: 02.11.2017
 * Time: 15:37
 */

namespace Organizations\OrgsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Organizations\OrgsBundle\Entity\Worker;

use Organizations\OrgsBundle\Form\WorkerForm;

class WorkerController extends Controller
{
    private $logger = null;

    private function getLogger()
    {
        if ($this->logger == null)
        {
            $this->logger = $this->get('logger');
        }
        return $this->logger;
    }

    public function workerInfoAction($orgId, $id)
    {
        $this->getLogger()->info("View info about Worker with id - " . $id);

        $em = $this->getDoctrine()->getEntityManager();
        $worker = $em->getRepository('OrgsBundle:Worker')->findOneBy(array('id' => $id));

        return $this->render('OrgsBundle:Worker:worker_info.html.twig', array('worker'=> $worker, "orgId" => $orgId));
    }

    public function workerCreateAction(Request $request, $orgId)
    {
        $this->getLogger()->info("Create form for Worker");

        $worker = new Worker();

        $form = $this->createForm(new WorkerForm(), $worker);

        if( $request->getMethod() == 'POST') {
            $form->submit($request);
            if($form->isValid()) {
                $this->getLogger()->err("Create new Worker is success");

                $em = $this->getDoctrine()->getEntityManager();

                $organization = $em->getRepository('OrgsBundle:Organization') ->findOneBy(array('id' => $orgId));
                $worker->setOrganization($organization);

                $em->persist($worker);
                $em->flush();

                $this->getLogger()->err("Worker created in database with id - " . $worker->getId());

                $this->addFlash(
                    'notice',
                    $this->get('translator')->trans('notice.create.worker.is.done',
                        array('%fullname%' => $worker->getFullName())));

                return $this->redirect($this->generateUrl('worker_info', array('orgId' => $orgId, 'id' => $worker->getId())));
            }
            else {
                $this->getLogger()->err("Form Worker is not correct");
            }
        }

        return $this->render('OrgsBundle:Worker:worker_new.html.twig', array('form' => $form->createView(), 'orgId' => $orgId));
    }

    public function workerEditAction(Request $request, $orgId, $id)
    {
        $this->getLogger()->info("Edit Worker with id - " . $id);

        $em = $this->getDoctrine()->getEntityManager();
        $worker = $em->getRepository('OrgsBundle:Worker') ->findOneBy(array('id' => $id));

        $form = $this->createForm(new WorkerForm(), $worker);

        if( $request->getMethod() == 'POST') {
            $form->submit($request);
            if($form->isValid()) {
                $this->getLogger()->err("Edit Worker with id: " . $id);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($worker);
                $em->flush();

                $this->getLogger()->err("Worker edit in database with id - " . $worker->getId());

                $this->addFlash(
                    'notice',
                    $this->get('translator')->trans('notice.editing.worker.successful',
                        array('%fullname%' => $worker->getFullName())));

                return $this->redirect($this->generateUrl('worker_info', array('orgId' => $orgId, 'id' => $worker->getId())));
            }
            else {
                $this->getLogger()->err("Form Worker is not correct");
            }
        }

        return $this->render('OrgsBundle:Worker:worker_edit.html.twig',
                                array('form' => $form->createView(), 'orgId' => $orgId, 'worker' => $worker));
    }

    public function workerRemoveAction($orgId, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $worker = $em->getRepository('OrgsBundle:Worker')->findOneBy(array('id' => $id));

        $em->remove($worker);
        $em->flush();

        $this->addFlash(
            'notice',
            $this->get('translator')->trans('notice.removing.worker.successful',
                array('%fullname%' => $worker->getFullName())));

        return $this->redirect($this->generateUrl('orgs_info_organization', array('id' => $orgId)));
    }

}