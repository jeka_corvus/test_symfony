<?php
/**
 * Created by PhpStorm.
 * User: eugene.kuzob
 * Date: 02.11.2017
 * Time: 15:37
 */

namespace Organizations\OrgsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Organizations\OrgsBundle\Entity\Organization;

use Organizations\OrgsBundle\Entity\OrganizationImportManager;
use Organizations\OrgsBundle\Entity\WorkerImportManager;

use Organizations\OrgsBundle\Form\OrganizationForm;

use Symfony\Component\Validator\Constraints\File;

use Organizations\OrgsBundle\Util\StoreXMLReader;

use Doctrine\ORM\Query\ResultSetMapping;

class OrganizationController extends Controller
{
    private $logger = null;

    private function getLogger()
    {
        if ($this->logger == null)
        {
            $this->logger = $this->get('logger');
        }
        return $this->logger;
    }
    
    public function organizationsListAction()
    {
        $this->getLogger()->info("View list Organizations");

        $em = $this->getDoctrine()->getEntityManager();
        $organizations = $em->getRepository('OrgsBundle:Organization') ->findAllOrderedById();

        $this->getLogger()->info("Search Organizations count: " . count($organizations));

        return $this->render('OrgsBundle:Organization:organizations_list.html.twig', array('organizations'=> $organizations));
    }

    public function organizationInfoAction($id)
    {
        $this->getLogger()->info("View info about Organization with id - " . $id);

        $em = $this->getDoctrine()->getEntityManager();
        $organization = $em->getRepository('OrgsBundle:Organization')->findOneBy(array('id' => $id));

        return $this->render('OrgsBundle:Organization:organization_info.html.twig', array('organization'=> $organization));
    }

    public function organizationCreateSuccessAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $organization = $em->getRepository('OrgsBundle:Organization') ->findOneBy(array('id' => $id));

        $this->getLogger()->info("Success create Organization - " . $organization->getId() . "-" . $organization->getTitle());

        return $this->render('OrgsBundle:Organization:organization_success_create.html.twig',
                                    array('organization'=> $organization));
    }

    public function organizationCreateAction(Request $request)
    {
        $this->getLogger()->info("Create form for Organization");

        $organization = new Organization();

        $form = $this->createForm(new OrganizationForm(), $organization);

        if( $request->getMethod() == 'POST') {
            $form->submit($request);
            if($form->isValid()) {
                $this->getLogger()->err("Create new Organization is success");

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($organization);
                $em->flush();

                $this->getLogger()->err("Organization created in database with id - " . $organization->getId());

                $this->addFlash(
                    'notice',
                    $this->get('translator')->trans('notice.create.organization.is.done', array('%title%' => $organization->getTitle()))
                );

                return $this->redirect($this->generateUrl('orgs_info_organization', array('id' => $organization->getId())));
            }
            else {
                $this->getLogger()->err("Form Organization is not correct");
            }
        }

        return $this->render('OrgsBundle:Organization:organization_new.html.twig', array('form'=> $form->createView()));
    }

    public function organizationEditAction(Request $request, $id)
    {
        $this->getLogger()->info("Edit Organization with id - " . $id);

        $em = $this->getDoctrine()->getEntityManager();
        $organization = $em->getRepository('OrgsBundle:Organization') ->findOneBy(array('id' => $id));

        $form = $this->createForm(new OrganizationForm(), $organization);

        if( $request->getMethod() == 'POST') {
            $form->submit($request);
            if($form->isValid()) {
                $this->getLogger()->err("Edit Organization with id: " . $id);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($organization);
                $em->flush();

                $this->getLogger()->err("Organization edit in database with id - " . $organization->getId());

                $this->addFlash(
                    'notice',
                    $this->get('translator')->trans('notice.editing.organization.successful',
                                                    array('%title%' => $organization->getTitle())));

                return $this->redirect($this->generateUrl('orgs_info_organization', array('id' => $organization->getId())));
            }
            else {
                $this->getLogger()->err("Form Organization is not correct");
            }
        }

        return $this->render('OrgsBundle:Organization:organization_edit.html.twig',
                                array('form'=> $form->createView(), 'organization' => $organization));
    }

    public function organizationEditSuccessAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $organization = $em->getRepository('OrgsBundle:Organization') ->findOneBy(array('id' => $id));

        $this->getLogger()->info("Success edit Organization - " . $organization->getId() . "-" . $organization->getTitle());

        return $this->render('OrgsBundle:Organization:organization_success_edit.html.twig',
            array('organization'=> $organization));
    }

    public function organizationRemoveAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $organization = $em->getRepository('OrgsBundle:Organization')->findOneBy(array('id' => $id));

        $em->remove($organization);
        $em->flush();

        $this->addFlash(
            'notice',
            $this->get('translator')->trans('notice.removing.organization.successful',
                array('%title%' => $organization->getTitle())));

        return $this->redirect($this->generateUrl('orgs_homepage'));
    }


    /* Импорт данных из XML порядка 1 Gb
     1. Сделать маасовую вставку через Doctrine, примерно по 100 записей, чтобы не дёргать часто БД
      1.1. проблема в том, что при вставке объекта в БД нужно проверять на существование его, а это уже дёргается БД
          1.1.1 попробовать использовать хранимые процедуры в MySQL
                    http://www.doctrine-project.org/2009/08/15/doctrine2-native-queries.html
                    данный метод не подходит, потому как каждый раз вызывается execute(), который уже работает с БД
                    и он не поддерживает множественный SQL (не проверял, но скорее всего так и есть)


     2. Генерить чистый SQL с insert or update и в конце либо скормить крону либо запустить самому shell команду
        https://symfony.com/doc/2.8/components/process.html
        https://stackoverflow.com/questions/36626282/call-mysql-command-in-symfony2-controller
     3. Поискать, возможно ли в Doctrine делать запросы чистым SQL и вставлять массово в БД по 100 или 1000 запросов сразу
    4. Создавать временные таблицы и заливать туда данные без проверки на уникальность с помощью массовой вставки Doctrine,
        а потом с помощью хранимой процедуры в MySQL перезалить данные по нужным таблицам.
        https://ruseller.com/lessons.php?id=1189
        https://dev.mysql.com/doc/refman/5.7/en/create-procedure.html
        https://stackoverflow.com/questions/12512868/how-to-execute-stored-procedures-with-symfony2-doctrine2

    Реализую №4
    */
    public function organizationsImportAction(Request $request)
    {
        $form = $this->createFormBuilder()
        ->add('file_import','file', array(
            'constraints' => new File(array('mimeTypes' => array('application/xml', 'text/xml'),
                                    'mimeTypesMessage' => $this->get('translator')->trans('format.file.must.be.xml')))))
        ->getForm();
        $arrayErrors = array();

        if( $request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->getLogger()->info("Import Organizations with workers from XML - START.");

                $infoImportFile = $form['file_import']->getData();
                $pathToFile = $infoImportFile->getPath() . DIRECTORY_SEPARATOR . $infoImportFile->getFileName();

                $xmlReader = new StoreXMLReader();

                $em = $this->getDoctrine()->getEntityManager();

                $em->getConnection()->beginTransaction();
                $organizationImportManager = new OrganizationImportManager($this->container);
                $workerImportManager = new WorkerImportManager($this->container);
                $maxCountObjectInsert = 100;
                $organizationImport = null;

                try {
                    $xmlReader->open($pathToFile);

                    $element = $xmlReader->getNextElement();
                    while (count($element) > 0)
                    {
                        switch ($element[0])
                        {
                            case 'org':
                                $organizationImportManager->mappingOrganizationFromArray($element[1], $arrayErrors);
                                $organizationImport = $organizationImportManager->getOrganizationImport();
                                if ($organizationImport == null) {
                                    $this->getLogger()->err("Problem with import Organization.");
                                    throw new \Exception($this->get('translator')->trans('error.import.problem.organization'));
                                }

                                $this->getLogger()->info("Import Organization - " . $organizationImport->getTitle() . " - is done.");

                                if (count($element = $xmlReader->getNextElement()) == 0)
                                    break;

                                break;

                            case 'user':
                                $workerImportManager->mappingWorkerFromArray($element[1], $organizationImport, $arrayErrors);
                                $workerImport = $workerImportManager->getWorkerImport();

                                if ($workerImport == null) {
                                    $this->getLogger()->err("Problem with import Worker.");
                                    throw new \Exception($this->get('translator')->trans('error.import.problem.worker'));
                                }

                                $this->getLogger()->info("Import Worker - " . $workerImport->getFullName() . " - is done.");

                                if (count($element = $xmlReader->getNextElement()) == 0)
                                    break;

                                break;

                            default:
                                $element = $xmlReader->getNextElement();
                                $organizationImport = null;
                                break;
                        }

                        if ($em->getUnitOfWork()->size() != 0 && $em->getUnitOfWork()->size() % $maxCountObjectInsert == 0)
                        {
                            $em->flush();
                            $em->clear();

                            $organizationImport = $em->merge($organizationImport);
                        }
                    }

                    $xmlReader->close();
                    $em->flush();
                    $em->clear();
                    $em->getConnection()->commit();

                    $this->getLogger()->info("Import is done.");

                } catch (\Exception $e) {
                    $em->getConnection()->rollBack();
                    $xmlReader->close();

                    $arrayErrors[] = array($e->getMessage());
                    return $this->render('OrgsBundle:Organization:organizations_import.html.twig',
                        array('form' => $form->createView(), 'arrayErrors' => $arrayErrors));
                }

                $em->getRepository('OrgsBundle:OrganizationImport')->doImportStoredProcedure();

                $this->addFlash(
                    'notice',
                    $this->get('translator')->trans('import.organizations.xml.successful'));

                if (count($arrayErrors) == 0) {
                    return $this->redirect($this->generateUrl('orgs_homepage'));
                }
            } else {
                $this->getLogger()->err("Form Import XML is not correct");
            }
        }

        return $this->render('OrgsBundle:Organization:organizations_import.html.twig',
                            array('form'=> $form->createView(), 'arrayErrors' => $arrayErrors));
    }

//    public function organizationsImportActionOld(Request $request)
//    {
//        $form = $this->createFormBuilder()
//        ->add('file_import','file', array(
//            'constraints' => new File(array('mimeTypes' => array('application/xml', 'text/xml'),
//                                    'mimeTypesMessage' => $this->get('translator')->trans('format.file.must.be.xml')))))
//        ->getForm();
//        $arrayErrors = array();
//
//        if( $request->getMethod() == 'POST') {
//            $form->submit($request);
//            if ($form->isValid()) {
//                $this->getLogger()->info("Import Organizations with workers from XML - START.");
//
//                $infoImportFile = $form['file_import']->getData();
//                $pathToFile = $infoImportFile->getPath() . DIRECTORY_SEPARATOR . $infoImportFile->getFileName();
//
//                $xmlReader = new StoreXMLReader();
//
//                $em = $this->getDoctrine()->getEntityManager();
//
//                $em->getConnection()->beginTransaction();
//                $organizationManager = new OrganizationManager($this->container);
//                $workerManager = new WorkerManager($this->container);
//                try {
//                    $xmlReader->open($pathToFile);
//
//                    $element = $xmlReader->getNextElement();
//                    while (count($element) > 0)
//                    {
//                        if ($element[0] == 'org')
//                        {
//                            $organizationManager->mappingOrganizationFromArray($element[1], $arrayErrors);
//                            $organization = $organizationManager->getOrganization();
//                            if ($organization == null) {
//                                $this->getLogger()->err("Problem with import Organization.");
//                                throw new \Exception($this->get('translator')->trans('error.import.problem.organization'));
//                            }
//
//                            $this->getLogger()->info("Import Organization - " . $organization->getTitle() . " - is done.");
//
//                            if (count($element = $xmlReader->getNextElement()) == 0)
//                                break;
//
//                            while ($element[0] == 'user') {
//                                $workerManager->mappingWorkerFromArray($element[1], $organization, $arrayErrors);
//                                $worker = $workerManager->getWorker();
//
//                                if ($worker == null) {
//                                    $this->getLogger()->err("Problem with import Worker.");
//                                    throw new \Exception($this->get('translator')->trans('error.import.problem.worker'));
//                                }
//
//                                $this->getLogger()->info("Import Worker - " . $worker->getFullName() . " - is done.");
//
//                                if (count($element = $xmlReader->getNextElement()) == 0)
//                                    break;
//                            }
//                        }
//                        else {
//                            $element = $xmlReader->getNextElement();
//                        }
//
//                    }
//
//                    $xmlReader->close();
//                    $em->flush();
//                    $em->getConnection()->commit();
//                    $this->getLogger()->info("Import is done.");
//
//                } catch (\Exception $e) {
//                    $em->getConnection()->rollBack();
//                    $xmlReader->close();
//
//                    $arrayErrors[] = array($e->getMessage());
//                    return $this->render('OrgsBundle:Organization:organizations_import.html.twig',
//                        array('form' => $form->createView(), 'arrayErrors' => $arrayErrors));
//                }
//
//                $this->addFlash(
//                    'notice',
//                    $this->get('translator')->trans('import.organizations.xml.successful'));
//
//                if (count($arrayErrors) == 0) {
//                    return $this->redirect($this->generateUrl('orgs_homepage'));
//                }
//            } else {
//                $this->getLogger()->err("Form Import XML is not correct");
//            }
//        }
//
//        return $this->render('OrgsBundle:Organization:organizations_import.html.twig',
//                            array('form'=> $form->createView(), 'arrayErrors' => $arrayErrors));
//    }
}