<?php

namespace Organizations\OrgsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OrgsBundle:Default:index.html.twig');
    }
}
