<?php

namespace Organizations\OrgsBundle\Entity;

/**
 * WorkerImportManager
 */
class WorkerImportManager
{
    private $workerImport;
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setWorkerImport(WorkerImport $workerImport) {
        $this->workerImport = $workerImport;
    }

    public function getWorkerImport() {
        return $this->workerImport;
    }

    public function __toString()
    {
        return "Worker: id - " . $this->workerImport->getId() . "; lastname - " . $this->workerImport->getLastname()
            . "; firstname - " . $this->workerImport->getАirstname() . "; middlename - " . $this->workerImport->getMiddlename()
            . "; birthday - " . $this->workerImport->getBirthday() . "; inn - " . $this->workerImport->getInn()
            . "; snils - " . $this->workerImport->getSnils();
    }

    public function mappingWorkerFromArray(array $arrayFields, OrganizationImport $organizationImport, &$arrayErrors)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        if (count($arrayFields) == 0) {
            throw new \Exception($this->get('translator')->trans('error.import.problem.worker'));
        }

        $this->workerImport = new WorkerImport();
        if (!isset($arrayFields['inn']) || $arrayFields['inn'] == "" || $organizationImport == null) {
            $fullName = $arrayFields['lastname'] . " " . $arrayFields['firstname'] . " " . $arrayFields['middlename'];
            $this->container->get('logger')->err("Problem with import Worker - " . $fullName);
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.worker.title',
                array('%title%' => $fullName)));
        }

        $this->workerImport->setLastname($arrayFields['lastname']);
        $this->workerImport->setFirstname($arrayFields['firstname']);
        $this->workerImport->setMiddlename($arrayFields['middlename']);
        $this->workerImport->setInn($arrayFields['inn']);
        $this->workerImport->setSnils($arrayFields['snils']);
        $this->workerImport->setOrganization($organizationImport);

        $validator = $this->container->get('validator');
        $errors = $validator->validate($this->workerImport);

        if (count($errors) > 0) {
            $arrayErrors[] = array($this->workerImport, $errors);

            $this->container->get('logger')->err("Worker - " . $this->workerImport->getFullName() . " - is not correct.");
            $this->container->get('logger')->err((string)$errors);
        }
        else {
            $em->persist($this->workerImport);
        }

        return $this->workerImport;
    }
}
