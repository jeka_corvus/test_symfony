<?php

namespace Organizations\OrgsBundle\Entity;

/**
 * OrganizationImportManager
 */
class OrganizationImportManager
{
    private $organizationImport;
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setOrganizationImport(OrganizationImport $org) {
        $this->organizationImport = $org;
    }

    public function getOrganizationImport() {
        return $this->organizationImport;
    }

     public function __toString()
    {
        return "Organization: id - " . $this->organizationImport->getId() . "; title - " . $this->organizationImport->getTitle()
            . "; ogrn - " . $this->organizationImport->getOgrn() . "; oktmo - " . $this->organizationImport->getOktmo();
    }

    public function mappingOrganizationFromArray(array $arrayFields, &$arrayErrors)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        if (count($arrayFields) == 0) {
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.organization'));
        }

        $this->organizationImport = new OrganizationImport();

        if (!isset($arrayFields['ogrn']) || $arrayFields['ogrn'] == "") {
            $this->container->get('logger')->err("Problem with import Organization - "
                . $arrayFields['displayName']);
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.organization.title',
                array('%title%' => $arrayFields['displayName'])));
        }

        $this->organizationImport->setTitle($arrayFields['displayName']);
        $this->organizationImport->setOgrn($arrayFields['ogrn']);
        $this->organizationImport->setOktmo($arrayFields['oktmo']);

        $validator = $this->container->get('validator');
        $errors = $validator->validate($this->organizationImport);

        if (count($errors) > 0) {
            $arrayErrors[] = array($this->organizationImport, $errors);

            $this->container->get('logger')->err("Organization - " . $this->organizationImport->getTitle() . " - is not correct.");
            $this->container->get('logger')->err((string)$errors);
        }
        else {
            $em->persist($this->organizationImport);
        }

        return $this->organizationImport;
    }
}
