<?php

namespace Organizations\OrgsBundle\Entity;

/**
 * WorkerManager
 */
class WorkerManager
{
    private $worker;
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setWorker(Worker $worker) {
        $this->worker = $worker;
    }

    public function getWorker() {
        return $this->worker;
    }

    public function __toString()
    {
        return "Worker: id - " . $this->worker->getId() . "; lastname - " . $this->worker->getLastname()
            . "; firstname - " . $this->worker->getАirstname() . "; middlename - " . $this->worker->getMiddlename()
            . "; birthday - " . $this->worker->getBirthday() . "; inn - " . $this->worker->getInn()
            . "; snils - " . $this->worker->getSnils();
    }

    public function mappingWorkerFromArray(array $arrayFields, Organization $organization, &$arrayErrors)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        if (count($arrayFields) == 0) {
            throw new \Exception($this->get('translator')->trans('error.import.problem.worker'));
        }

        $this->worker = new Worker();
        if (isset($arrayFields['inn']) && $arrayFields['inn'] != "") {
//            $this->worker = $em->getRepository('OrgsBundle:Worker')
//                ->findOneBy(array('inn' => $arrayFields['inn']));
//            if ($this->worker == null)
//            {
//                $this->worker = new Worker();
//            }
        }
        else {
            $fullName = $arrayFields['lastname'] . " " . $arrayFields['firstname'] . " " . $arrayFields['middlename'];
            $this->container->get('logger')->err("Problem with import Worker - " . $fullName);
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.worker.title',
                array('%title%' => $fullName)));
        }

        $this->worker->setLastname($arrayFields['lastname']);
        $this->worker->setFirstname($arrayFields['firstname']);
        $this->worker->setMiddlename($arrayFields['middlename']);
        $this->worker->setInn($arrayFields['inn']);
        $this->worker->setSnils($arrayFields['snils']);
        $this->worker->setOrganization($organization);

//        $validator = $this->container->get('validator');
//        $errors = $validator->validate($this->worker);

        $errors = array();
        if (count($errors) > 0) {
            $arrayErrors[] = array($this->worker, $errors);

            $this->container->get('logger')->err("Worker - " . $this->worker->getFullName() . " - is not correct.");
            $this->container->get('logger')->err((string)$errors);
        }
        else {
            $em->persist($this->worker);
//            $em->flush();
        }

        return $this->worker;
    }
}
