<?php

namespace Organizations\OrgsBundle\Entity;

/**
 * OrganizationManager
 */
class OrganizationManager
{
    private $organization;
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setOrganization(Organization $org) {
        $this->organization = $org;
    }

    public function getOrganization() {
        return $this->organization;
    }

     public function __toString()
    {
        return "Organization: id - " . $this->organization->getId() . "; title - " . $this->organization->getTitle()
            . "; ogrn - " . $this->organization->getOgrn() . "; oktmo - " . $this->organization->getOktmo();
    }

    public function mappingOrganizationFromArray(array $arrayFields, &$arrayErrors)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        if (count($arrayFields) == 0) {
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.organization'));
        }

        $this->organization = new Organization();

        if (isset($arrayFields['ogrn']) && $arrayFields['ogrn'] != "") {
//            $this->organization = $em->getRepository('OrgsBundle:Organization')
//                ->findOneBy(array('ogrn' => $arrayFields['ogrn']));
//            if ($this->organization == null)
//            {
//                $this->organization = new Organization();
//            }
        }
        else {
            $this->container->get('logger')->err("Problem with import Organization - "
                . $arrayFields['displayName']);
            throw new \Exception($this->container->get('translator')->trans('error.import.problem.organization.title',
                array('%title%' => $arrayFields['displayName'])));
        }

        $this->organization->setTitle($arrayFields['displayName']);
        $this->organization->setOgrn($arrayFields['ogrn']);
        $this->organization->setOktmo($arrayFields['oktmo']);

//        $validator = $this->container->get('validator');
//        $errors = $validator->validate($this->organization);

        $errors = array();
        if (count($errors) > 0) {
            $arrayErrors[] = array($this->organization, $errors);

            $this->container->get('logger')->err("Organization - " . $this->organization->getTitle() . " - is not correct.");
            $this->container->get('logger')->err((string)$errors);
        }
        else {
            $em->persist($this->organization);
//            $em->flush();
        }

        return $this->organization;
    }
}
