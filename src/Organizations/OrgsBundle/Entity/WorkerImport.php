<?php

namespace Organizations\OrgsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkerImport
 */
class WorkerImport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $middlename;

    /**
     * @var \DateTime
     */
    private $birthday;

    /**
     * @var string
     */
    private $inn;

    /**
     * @var string
     */
    private $snils;

    /**
     * @var \Organizations\OrgsBundle\Entity\OrganizationImport
     */
    private $organization;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return WorkerImport
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return WorkerImport
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     * @return WorkerImport
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string 
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return WorkerImport
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set inn
     *
     * @param string $inn
     * @return WorkerImport
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn
     *
     * @return string 
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set snils
     *
     * @param string $snils
     * @return WorkerImport
     */
    public function setSnils($snils)
    {
        $this->snils = $snils;

        return $this;
    }

    /**
     * Get snils
     *
     * @return string 
     */
    public function getSnils()
    {
        return $this->snils;
    }

    /**
     * Set organization
     *
     * @param \Organizations\OrgsBundle\Entity\OrganizationImport $organization
     * @return WorkerImport
     */
    public function setOrganization(\Organizations\OrgsBundle\Entity\OrganizationImport $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \Organizations\OrgsBundle\Entity\OrganizationImport 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    public function getFullName()
    {
        return $this->lastname . " " . $this->firstname . " " . $this->middlename;
    }

    public function __toString()
    {
        return "Worker: id - " . $this->id . "; lastname - " . $this->lastname . "; firstname - " . $this->firstname
            . "; middlename - " . $this->middlename . "; birthday - " . $this->birthday . "; inn - " . $this->inn
            . "; snils - " . $this->snils;
    }
}
