<?php

namespace Organizations\OrgsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrganizationImport
 */
class OrganizationImport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $ogrn;

    /**
     * @var string
     */
    private $oktmo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workers_import;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workers_import = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return OrganizationImport
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ogrn
     *
     * @param string $ogrn
     * @return OrganizationImport
     */
    public function setOgrn($ogrn)
    {
        $this->ogrn = $ogrn;

        return $this;
    }

    /**
     * Get ogrn
     *
     * @return string 
     */
    public function getOgrn()
    {
        return $this->ogrn;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     * @return OrganizationImport
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string 
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Add workers_import
     *
     * @param \Organizations\OrgsBundle\Entity\WorkerImport $workersImport
     * @return OrganizationImport
     */
    public function addWorkersImport(\Organizations\OrgsBundle\Entity\WorkerImport $workersImport)
    {
        $this->workers_import[] = $workersImport;

        return $this;
    }

    /**
     * Remove workers_import
     *
     * @param \Organizations\OrgsBundle\Entity\WorkerImport $workersImport
     */
    public function removeWorkersImport(\Organizations\OrgsBundle\Entity\WorkerImport $workersImport)
    {
        $this->workers_import->removeElement($workersImport);
    }

    /**
     * Get workers_import
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkersImport()
    {
        return $this->workers_import;
    }

    public function __toString()
    {
        return "Organization: id - " . $this->id . "; title - " . $this->title . "; ogrn - " . $this->ogrn
            . "; oktmo - " . $this->oktmo;
    }
}
