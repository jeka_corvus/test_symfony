<?php

namespace Organizations\OrgsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Organization
 */
class Organization
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $ogrn;

    /**
     * @var string
     */
    private $oktmo;

    public function __construct()
    {
        $this->workers = new ArrayCollection();
    }
        /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Organization
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ogrn
     *
     * @param string $ogrn
     * @return Organization
     */
    public function setOgrn($ogrn)
    {
        $this->ogrn = $ogrn;

        return $this;
    }

    /**
     * Get ogrn
     *
     * @return string 
     */
    public function getOgrn()
    {
        return $this->ogrn;
    }

    /**
     * Set oktmo
     *
     * @param string $oktmo
     * @return Organization
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get oktmo
     *
     * @return string 
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Add workers
     *
     * @param \Organizations\OrgsBundle\Entity\Worker $workers
     * @return Organization
     */
    public function addWorker(\Organizations\OrgsBundle\Entity\Worker $workers)
    {
        $this->workers[] = $workers;

        return $this;
    }

    /**
     * Remove workers
     *
     * @param \Organizations\OrgsBundle\Entity\Worker $workers
     */
    public function removeWorker(\Organizations\OrgsBundle\Entity\Worker $workers)
    {
        $this->workers->removeElement($workers);
    }

    /**
     * Get workers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkers()
    {
        return $this->workers;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workers;

    public function __toString()
    {
        return "Organization: id - " . $this->id . "; title - " . $this->title . "; ogrn - " . $this->ogrn
            . "; oktmo - " . $this->oktmo;
    }
}
