<?php
/**
 * Created by PhpStorm.
 * User: eugene.kuzob
 * Date: 13.11.2017
 * Time: 13:27
 */

namespace Organizations\OrgsBundle\Util;

class StoreXMLReader
{
    private $reader;

    public function open($pathToFile)
    {
        $this->reader = new \XMLReader();
        $this->reader->open($pathToFile);
    }

    public function close()
    {
        $this->reader->close();
    }

    public function getNextElement()
    {
        $result = array();

        do {
            if (!$this->reader->read())
            {
                return $result;
            }
        } while ($this->reader->nodeType != \XMLReader::ELEMENT);


        $result[0] = $this->reader->name;
        $result[1] = $this->readAttributesFromXMLNode();

        return $result;
    }

    private function readAttributesFromXMLNode()
    {
        $arrayAttributes = array();

        if ($this->reader->hasAttributes)
        {
            $attributeCount = $this->reader->attributeCount;

            for ($i = 0; $i < $attributeCount; $i++)
            {
                $this->reader->moveToAttributeNo($i);
                $arrayAttributes[$this->reader->name] = $this->reader->value;
            }
            $this->reader->moveToElement();
        }
        return $arrayAttributes;
    }

}