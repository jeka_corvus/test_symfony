<?php
/**
 * Created by PhpStorm.
 * User: eugene.kuzob
 * Date: 03.11.2017
 * Time: 10:45
 */

namespace Organizations\OrgsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrganizationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder->add('title', "text", array("label" => "Наименование", "required" => true));
//        $builder->add('ogrn', "text", array("label" => "ОГРН", "required" => true));
//        $builder->add('oktmo', "text", array("label" => "ОКТМО", "required" => true));
        $builder->add('title', "text");
        $builder->add('ogrn', "text");
        $builder->add('oktmo', "text");
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Organizations\OrgsBundle\Entity\Organization',
        );
    }

    public function getName()
    {
        return 'organization';
    }
}