<?php
/**
 * Created by PhpStorm.
 * User: eugene.kuzob
 * Date: 03.11.2017
 * Time: 10:45
 */

namespace Organizations\OrgsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\FormBuilderInterface;

class WorkerForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder->add('title', "text", array("label" => "Наименование", "required" => true));
//        $builder->add('ogrn', "text", array("label" => "ОГРН", "required" => true));
//        $builder->add('oktmo', "text", array("label" => "ОКТМО", "required" => true));
        $builder->add('lastname', "text");
        $builder->add('firstname', "text");
        $builder->add('middlename', "text");
        $builder->add('birthday', BirthdayType::class, array(
                                'empty_value' => array('year' => 'Year', 'month'=>'Month', 'day'=>'Day')));
        $builder->add('inn', "text");
        $builder->add('snils', "text");
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Organizations\OrgsBundle\Entity\Worker',
        );
    }

    public function getName()
    {
        return 'worker';
    }
}