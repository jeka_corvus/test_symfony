﻿CREATE DATABASE IF NOT EXISTS symfony2_test;

CREATE TABLE `organization`
(
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `ogrn` VARCHAR(255) NOT NULL UNIQUE ,
  `oktmo` VARCHAR(100) NOT NULL UNIQUE ,
  PRIMARY KEY (`id`),
  CONSTRAINT UC_OGRN UNIQUE (`ogrn`),
  CONSTRAINT UC_OKTMO UNIQUE (`oktmo`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `worker`
(
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `organization_id` BIGINT(20) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(255) NOT NULL,
  `middlename` VARCHAR(255) NOT NULL,
  `birthday` DATETIME NULL,
  `inn` VARCHAR(50) NOT NULL ,
  `snils` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`),
  KEY `worker_organization_id_idx` (`organization_id`),
  CONSTRAINT UC_INN UNIQUE (`inn`),
  CONSTRAINT UC_SNILS UNIQUE (`snils`),
  CONSTRAINT `organization_worker_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


#----------------- Tables and stored procedures for import

CREATE TABLE `organization_import`
(
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `ogrn` VARCHAR(255) NOT NULL UNIQUE ,
  `oktmo` VARCHAR(100) NOT NULL UNIQUE ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `worker_import`
(
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `organization_id` BIGINT(20) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(255) NOT NULL,
  `middlename` VARCHAR(255) NOT NULL,
  `birthday` DATETIME NULL,
  `inn` VARCHAR(50) NOT NULL ,
  `snils` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`),
  KEY `worker_import_organization_import_id_idx` (`organization_id`),
  CONSTRAINT `organization_import_worker_import_id` FOREIGN KEY (`organization_id`) REFERENCES `organization_import` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP PROCEDURE IF EXISTS `migrate_data_from_orgs_import_to_orgs`;

DELIMITER //
CREATE PROCEDURE `migrate_data_from_orgs_import_to_orgs` ()
LANGUAGE SQL
DETERMINISTIC
  SQL SECURITY DEFINER
  COMMENT 'A procedure for migrate data about organizations from import table to real table.'
  BEGIN
    DECLARE done_orgs INT DEFAULT FALSE;

    DECLARE org_import_id, org_id INT;
    DECLARE org_import_title, org_import_ogrn, org_import_oktmo VARCHAR(255);
    DECLARE orgs_import_cur CURSOR FOR SELECT `id`, `title`, `ogrn`, `oktmo` FROM organization_import;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done_orgs = TRUE;

    OPEN orgs_import_cur;

    WHILE done_orgs = FALSE DO
      FETCH orgs_import_cur INTO org_import_id, org_import_title, org_import_ogrn, org_import_oktmo;

      INSERT INTO organization (`title`, `ogrn`, `oktmo`) VALUES (org_import_title, org_import_ogrn, org_import_oktmo)
      ON DUPLICATE KEY UPDATE `title`=org_import_title;

      SELECT `id` INTO org_id FROM organization WHERE `ogrn`=org_import_ogrn;

      CALL migrate_data_from_workers_import_to_workers(org_import_id, org_id);

    END WHILE;

    CLOSE orgs_import_cur;

    DELETE FROM organization_import;
    ALTER TABLE organization_import AUTO_INCREMENT = 1;
    ALTER TABLE worker_import AUTO_INCREMENT = 1;


  END//





DROP PROCEDURE IF EXISTS `migrate_data_from_workers_import_to_workers`;

CREATE PROCEDURE `migrate_data_from_workers_import_to_workers` (IN org_import_id INT, IN org_id INT)
LANGUAGE SQL
DETERMINISTIC
  SQL SECURITY DEFINER
  COMMENT 'A procedure for migrate data about workers from import table to real table.'
  BEGIN
    DECLARE done_workers INT DEFAULT FALSE;

    DECLARE worker_import_id, worker_id INT;
    DECLARE worker_import_birthday TIMESTAMP;
    DECLARE worker_import_lastname, worker_import_firstname, worker_import_middlename, worker_import_inn, worker_import_snils VARCHAR(255);
    DECLARE workers_import_cur CURSOR FOR
      SELECT `id`, `lastname`, `firstname`, `middlename`, `birthday`, `inn`, `snils` FROM worker_import WHERE `organization_id`=org_import_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done_workers = TRUE;


    OPEN workers_import_cur;

    WHILE done_workers = FALSE DO
      FETCH workers_import_cur
      INTO worker_import_id, worker_import_lastname, worker_import_firstname, worker_import_middlename,
        worker_import_birthday, worker_import_inn, worker_import_snils;

      INSERT INTO worker (`organization_id`, `lastname`, `firstname`, `middlename`, `birthday`, `inn`, `snils`)
      VALUES (org_id, worker_import_lastname, worker_import_firstname, worker_import_middlename, worker_import_birthday, worker_import_inn, worker_import_snils)
      ON DUPLICATE KEY UPDATE `lastname`=worker_import_lastname, `firstname`=worker_import_firstname,
        `middlename`=worker_import_middlename, `birthday`=worker_import_birthday;

    END WHILE;

    CLOSE workers_import_cur;

  END//

